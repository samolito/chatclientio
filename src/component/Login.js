import React, { Component } from 'react';
import '../styles/loginout.css'
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import axios from 'axios'
const ENPOINT="http://localhost:4400/api/user/login"
class Login extends Component{  
    state={
      email:'',
      password:''
    }

    onhandleChange=event=>{
      this.setState({
        [event.target.name]:event.target.value
      })
    }

      onLogin=(event)=>{
        event.preventDefault()
        
      axios.post(ENPOINT,{
          email:this.state.email,
          password:this.state.password
        })
        .then((response)=>{
          alert(response.data.message)
        })
        .catch((err)=>{
          console.log(err)
        })
        console.log(`email && Password :${this.state.email} :${this.state.password}`)
  }





  render(){
      const {email,password} =this.state
  return (
      <div className="contianer">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title text-center mb-4 mt-1">Sign in</h4>
                    <form onSubmit={this.onLogin}>
                        <div className="form-group">
                        <div className="input-group">
                          <div className="input-group-prepend">
                              <span className="input-group-text"> <i className="fa fa-user"></i> </span>
                          </div>
                          <input name="email"
                           className="form-control" 
                           placeholder="Email " 
                           type="email"
                           value={email}
                           onChange={this.onhandleChange}/>
                        </div> 
                        </div> 
                        <div className="form-group">
                        <div className="input-group">
                          <div className="input-group-prepend">
                              <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
                          </div>
                            <input 
                            name='password'
                            autoComplete=""
                            className="form-control"
                             placeholder="******" 
                             type="password"
                             value={password}
                             onChange={this.onhandleChange}
                             />
                        </div> 
                        </div>
                        <div className="form-group">
                        <button  type="submit" className="btn btn-primary btn-block"> Login  </button>
                        </div> 
                        <p className="text-center"><Link className="btn">Forgot password?</Link></p>
                  </form>
                </div>
            </div>
          </div>
  );
} 
}
export default Login