import React,{Component} from 'react'

class ChatMessageList extends Component{



 
 
 
    render(){
    return(
        <div className="px-4 py-5 chat-box bg-white">
        <div className="media w-50 mb-2"><img src="https://res.cloudinary.com/mhmd/image/upload/v1564960395/avatar_usae7z.svg" alt="user" width="50" className="rounded-circle"/>
          <div className="media-body ml-2">
            <div className="bg-light rounded py-2 px-3 mb-2">
              <p className="text-small mb-0 text-muted">Test which is a new approach all solutions</p>
            </div>
            <p className="small text-muted">12:00 PM | Aug 13</p>
          </div>
        </div>
          <div className="media w-50 ml-auto mb-3">
            <div className="media-body">
              <div className="bg-primary rounded py-2 px-3 mb-2">
                <p className="text-small mb-0 text-white">Apollo University, Delhi, India Test</p>
              </div>
              <p className="small text-muted">12:00 PM | Aug 13</p>
            </div>
          </div>
      </div>
         );
    }
}

export default ChatMessageList;