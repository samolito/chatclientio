import React,{ Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css'
import { Link } from "react-router-dom/cjs/react-router-dom.min";
export default class Navbar extends Component{





    render(){

        return(
            <div>
            <nav className="navbar navbar-expand-lg navbar-light">
                <Link className="navbar-brand" href="#"><strong>Samolito</strong></Link>
               
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="basicExampleNav">

                 
                    <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <Link className="nav-link" >Create Chatroom </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link font-weight-bold" id="chat">Logout</Link>
                    </li>

                    </ul>

                    <form className="form-inline">
                    <div className="md-form my-0">
                        <input className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"/>
                    </div>
                    </form>
                </div>

                </nav>
            </div> 
        )
    }
}