import React,{Component} from 'react'
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import onlineIcon from '../images/onlineIcon.png'
class UserList extends Component{



 
 
 
    render(){
                return(
                  <div className="col-3 px-0 ">
                  <div className="user-list bg-white">
            
                    <div className="bg-gray px-4 py-2 bg-light">
                      <p className="h5 mb-0 py-1">Active users</p>
                    </div>
            
                    <div className="messages-box">
                      <div className="list-group rounded-0">
                        <Link  className="list-group-item list-group-item-action list-group-item-light rounded-0">
                              <div className="d-flex align-items-center justify-content-between mb-1">
                                <h6 className="mb-0">Jason Doe </h6><img src={onlineIcon} alt="online"/>
                              </div>
                        </Link>

                      </div>
                    </div>
                  </div>
                  
                </div>
         );
    }
}

export default UserList;