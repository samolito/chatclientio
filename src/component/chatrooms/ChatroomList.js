import React,{Component} from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
const ENPOINT="http://localhost:4400/chatroom/getchatrooms"
class ChatroomList extends Component{

    state = {
        chatrooms: []
      }
        componentDidMount(){
          //this.setState({chatrooms:this.displayChatrooms})
          this.displayChatrooms()
        }

      displayChatrooms(){
        axios.get(ENPOINT,{})
        .then((response)=>{
          this.setState({chatrooms:response.data.chatrooms})
          console.log(this.state.chatrooms)
        })
        .catch((err)=>{
          console.log(`Error : ${err}`)
        })
      }

              // <div classNameName="App">

              //           <h4>Chatroom List</h4>

              //           <ul classNameName="list-group">
              //             {
              //             chatrooms && chatrooms.map((chatroom, index) => (
              //                 <li classNameName="list" key={index} >
              //                   {value.name}
              //                 </li>
              //               ))
              //               }
              //           </ul>
              // </div>
 
    render(){

        //const { chatrooms } =this.state
            return(
                <div className="col-2 px-0 bg-white">
                    <div className="chatlist bg-white">
                          <div className="bg-gray px-4 py-2 bg-light">
                              <p className="h5 mb-0 py-1">Chatrooms</p> 
                        </div>
                        <div className="list-group rounded-0">
                           {this.state.chatrooms&& this.state.chatrooms.map((chatroom,index)=>(
                            <Link key={chatroom._id} className="list-group-item list-group-item-action list-group-item-light rounded-0">
                                  <div className="d-flex align-items-center justify-content-between mb-1">
                                    <h6 className="mb-0" >{chatroom.name}</h6>
                                  </div>
                            </Link>
                       )) }
                          </div>
                    </div>
                
                </div>
         );
    }
}

export default ChatroomList;