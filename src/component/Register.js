import React, { Component } from 'react';
import '../styles/loginout.css'
import axios from 'axios';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
const ENPOINT="http://localhost:4400/api/user/register"
class Register extends Component{
  state={
    email:'',
    username:'',
    password:''
  }


  onhandleChange = event=>{
    this.setState({
      [event.target.name]:event.target.value,
    
    })
  }
  
  onRegister=(event)=>{
    event.preventDefault()
    axios.post(ENPOINT,{
      username:this.state.username,
      email:this.state.email,
      password:this.state.password
      
    })
    .then((response)=>{
      alert(response.data.token)
    })
    .catch((err)=>{
      console.log(err)
    })
    console.log(`username :${this.state.username} 
                  email :${this.state.email} and password : ${this.state.password}`)     
  }
  render(){
    const {email,password,username} =this.state
  return (
      <div className="contianer">
            <div className="card">
                <div className="card-body">
                    <h4 className="card-title text-center mb-4 mt-1">Register</h4>
                    <form onSubmit={this.onRegister}>
                        <div className="input-group mt-3">
                          <div className="input-group-prepend">
                              <span className="input-group-text"> <i className="fa fa-user"></i> </span>
                          </div>
                          <input 
                            name="username"
                            className="form-control" 
                            placeholder="username " 
                            type="username"
                            value={username}
                            onChange={this.onhandleChange}/>
                        </div> 
                        <div className="input-group mt-3">
                          <div className="input-group-prepend">
                              <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
                          </div>
                            <input 
                            name="email"
                            className="form-control" 
                            placeholder="email" 
                            type="email"
                            value={email}
                            onChange={this.onhandleChange}/>
                        </div> 
                        <div className="input-group mt-3">
                          <div className="input-group-prepend">
                              <span className="input-group-text"> <i className="fa fa-lock"></i> </span>
                          </div>
                            <input
                            autoComplete=""
                            name="password"
                            className="form-control"
                             placeholder="******" 
                             type="password"
                             value={password}
                            onChange={this.onhandleChange}/>
                        </div> 
                        <div className="form-group mt-3">
                        <button type="submit" className="btn btn-primary btn-block"> Register  </button>
                        </div> 
                        <p className="text-center"><Link className="btn">Go to signup</Link></p>  
                        </form>       
                </div>
            </div>
          </div>
  );
} 
}
export default Register