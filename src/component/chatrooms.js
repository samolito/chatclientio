import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/chatrooms.css'
import Navabr from '../component/chatrooms/navbar'
import ChatForm from './chatrooms/ChatForm';
import ChatMessageList from './chatrooms/ChatMessagesList';
import UserList from './chatrooms/UserList';
import ChatroomList from './chatrooms/ChatroomList';
class Chatroom extends Component{

  
  render(){
   
            return (
              
              <div className="chatbody">
               
              <div className="container py-5 ">
              <Navabr/>
              <div className="row rounded-lg overflow-hidden shadow">
                 
                  <ChatroomList/>

                   <UserList/>
                
                        
                    <div className="col-7 px-0">
                  
                  <ChatMessageList/>
                  <ChatForm/>
                </div>
              </div>
            </div>
            </div>
            );
  }
}
export default Chatroom