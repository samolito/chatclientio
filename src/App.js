import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom"
import Login from './component/Login';
import Chatroom from './component/chatrooms';
import Register from './component/Register';
//const ENPOINT="http://localhost:4400/chatroom/getchatrooms"
class App extends Component{

   render(){
  return (
             <BrowserRouter>
              

                <Switch>
                      <Route path="/" exact><Login/></Route>
                      <Route path="/login" exact><Login/></Route>
                      <Route path="/Register"exact><Register/></Route>
                      <Route path="/chatroom"exact><Chatroom/></Route>
                  </Switch>
             </BrowserRouter>
      )
  
}
}
export default App 